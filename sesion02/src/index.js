import React from 'react';
import ReactDom from 'react-dom';
import './styles/main.css';
import Kitten from "./components/Kitten";


ReactDom.render( <Kitten/>, document.getElementById('root') );
