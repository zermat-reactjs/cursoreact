import React from 'react';

function FunctionalTitle() {
    return <h1>My Functional Title Component</h1>;
}

export default FunctionalTitle;
