import React, {Component} from 'react';
import Title from "./Title";

class Kitten extends Component {

    state = {
      url: '',
      id: 'nope'
    };

    componentDidMount() {
        fetch('https://api.thecatapi.com/v1/images/search?size=full')
            .then(resp => resp.json())
            .then(response => {
                console.log(response);
                this.setState(response[0]);
            });
    }

    render() {
        return (<div>
            <Title/>
            <img alt="Gatito" src={this.state.url}/>
        </div>)
    }
}

export default Kitten;
