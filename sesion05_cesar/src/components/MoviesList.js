import React, {Component} from 'react';
import {MDBContainer, MDBIcon, MDBRow} from "mdbreact";
import MovieItem from "./MovieItem";
import API from "../utils/API";

class MoviesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            response: true
        };

        this.createItems = this.createItems.bind(this);
        this.inputHandler = this.inputHandler.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.errorMessage = this.errorMessage.bind(this);
    }

    componentDidMount() {
    }

    fetchData (title) {

        API.getMovies(title)
            .then(resp => {
                console.log('movies---> ',resp);

                if (resp.Response === "True") {
                    this.setState( {list: resp.Search, response: true} );
                } else {
                    this.setState( {list: [], response: false} );
                }
            });
    }

    inputHandler (evt) {
        const title = evt.target.value;

        if (title.length >= 3) {
            this.fetchData(title);
        } else {
            this.setState({list: [], response: false});
        }
    }

    createItems () {
        return this.state.list.map( movie => <MovieItem movie={movie} /> );
    };

    errorMessage () {
        return this.state.response === false ? <h2>Sorry, we have an error!</h2> : '';
    }

    render() {
        return (
            <MDBContainer>
                <h1>Search for a movie!</h1>
                {this.errorMessage()}
                <MDBRow md="12" lg="12">
                    <div className="input-group md-form form-sm form-1 pl-0">
                        <div className="input-group-prepend">
                          <span className="input-group-text blue lighten-3" id="basic-text1">
                            <MDBIcon className="text-white" icon="search" />
                          </span>
                        </div>
                        <input onChange={this.inputHandler} className="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search" />
                    </div>
                </MDBRow>
                <MDBRow>
                    {this.createItems()}
                </MDBRow>
            </MDBContainer>)
    }
}

export default MoviesList;
