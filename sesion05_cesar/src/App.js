import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.scss';
import './styles/main.scss';
import MoviesList from "./components/MoviesList";
import MovieDetail from "./components/MovieDetail/";

function App() {
  return (
    <Router>
        <Route path='/' exact component={MoviesList}/>
        <Route path='/movie/:id' component={MovieDetail} />
    </Router>
  );
}

export default App;
