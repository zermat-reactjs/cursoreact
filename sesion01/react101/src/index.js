import React from 'react';
import ReactDom from 'react-dom';

let myNumber = 0;

function add() {
  myNumber++;
  document.querySelector('#title').innerHTML = myNumber.toString();
}

const elem = <div>
  <h1 id='title'>{myNumber}</h1>
  <input type="button" value="PLUS 1" onClick={add}/>
</div>;
ReactDom.render(elem, document.querySelector('#root'));
