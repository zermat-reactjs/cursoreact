import React, {Component} from 'react';
import {Card} from "react-bootstrap";


class MovieItem extends Component{
    constructor (props) {
        super(props)

        this.state = {
            msg: ''
        }
        this.updateMsg = this.updateMsg.bind(this);
    }

    componentDidMount() {
        this.updateMsg();
    }

    updateMsg () {
        const msg = parseInt(this.props.movie.Year) < 2010 ? 'Old movie' : 'New Movie';
        console.log('---', msg)
        this.setState({msg: msg})
    }

    render() {
        const {Title, Year, Poster} = this.props.movie;
        return (
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={Poster} />
                <Card.Body>
                    <Card.Title>{Title}</Card.Title>
                    <Card.Text>
                        {Year}
                        {this.state.msg}
                    </Card.Text>
                </Card.Body>
            </Card>
        )
    }
}

export default MovieItem;
