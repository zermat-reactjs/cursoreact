import React, {Component} from 'react';
import axios from 'axios';
import MovieItem from "./MovieItem";

class MoviesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.createItems = this.createItems.bind(this);
    }

    componentDidMount() {
        axios.get('http://www.omdbapi.com/?s=batman&apikey=a4a947a3')
            .then(resp => {
               //console.log(resp);
               this.setState( {list: resp.data.Search} );
            });
    }

    createItems () {
        console.log('movies ',this.state.list);
        const allMovies = this.state.list.map( movie => <MovieItem key={movie.imdbID} movie={movie}/> );
        return allMovies;
    };


    render() {
        return (<div>
            <h1>Movies List</h1>
            {this.createItems()}
        </div>)
    }
}

export default MoviesList;
