import React, {Component} from 'react';
import CoolButton from "./CoolButton";

class ButtonsList extends Component {
    constructor (props) {
        super(props);

        this.state = {
            selectedButton: null
        };

        this.createButtons = this.createButtons.bind(this);
        this.setSelectedButton = this.setSelectedButton.bind(this);
    }

    setSelectedButton (index) {
        console.log('index--->', index);
        this.setState({selectedButton: index});
    }

    createButtons () {
        let allButtons = [];
        for (let i = 0; i < this.props.amount; i++) {
            allButtons.push( <CoolButton text={ 'Button ' + i } handler={this.setSelectedButton}/> )
        }

        return allButtons;
    }

    render() {
        return (<div>
            {this.createButtons()}
        </div>)
    }
}

export default ButtonsList;
