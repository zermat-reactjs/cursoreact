import React, {Component} from 'react';
import CoolMenuItem from "./CoolMenuItem";

class MenuList extends Component{
    constructor (props) {
        super(props);
        this.state = {
            menu: [
                {
                    label: 'Home',
                    path: 'http://www.google.com.mx'
                },
                {
                    label: 'Products',
                    path: 'http://www.dedica.com.mx'
                },
                {
                    label: 'Contact',
                    path: 'http://www.apple.com'
                },
            ]
        }
        this.createMenuItems = this.createMenuItems.bind(this);
    }

    createMenuItems () {
        return this.state.menu.map( item => <CoolMenuItem label={item.label} path={item.path} /> );
    }

    render() {
        return (<ul>
            {this.createMenuItems()}
        </ul>)
    }
}

export default MenuList;
