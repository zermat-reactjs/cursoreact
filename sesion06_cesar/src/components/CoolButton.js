import React, {Component} from 'react';

class CoolButton extends Component{
    constructor (props) {
        super(props);
        this.clickHandler = this.clickHandler.bind(this);
    }

    static defaultProps = {
      text: "My cool button here!",
      handler: () => {console.log('CLICKED!!!')}
    };

    clickHandler() {
        this.props.handler(this.props.text);
    }

    render() {
        return <button onClick={this.clickHandler}>{this.props.text}</button>
    }
}

export default CoolButton;
