import React, {Component} from 'react';

class CoolMenuItem extends Component{
    render() {
        return <li><a href={this.props.path} target="_blank">{this.props.label}</a></li>
    }
}

export default CoolMenuItem;
