import React from 'react';
import logo from './logo.svg';
import './App.css';
import Items from "./components/Items";
import CoolButton from "./components/CoolButton";
import ButtonsList from "./components/ButtonsList";
import MenuList from "./components/MenuList";

function App() {
  return (
    <div className="App">
      <Items title='Mi Título'>
        <h1>hola</h1>
        <h2>good morning</h2>
        <CoolButton/>
        <CoolButton text='Hola'/>
      </Items>
      <hr/>
      <ButtonsList amount='3'/>
      <hr/>
      <MenuList/>
    </div>
  );
}

export default App;
