import React, {Component} from 'react';
import Proptypes from 'prop-types'

class SearchBar extends Component {

    constructor (props) {
        super(props);

        this.state = {
            searchTerm: ''
        };

        this.searchClick = this.searchClick.bind(this);
        this.inputHandler = this.inputHandler.bind(this);
    }

    static defaultProps = {
      searchHandler: () => {console.log('HOLA')}
    };

    searchClick () {
        this.props.searchHandler(this.state.searchTerm);
    }

    inputHandler (evt) {
        this.setState({searchTerm: evt.target.value});
    }

    render() {
        return (
            <form className="form-inline ">
                <input onChange={this.inputHandler} className="form-control col-md-8 offset-1" type="search" placeholder="Search" aria-label="Search City"/>
                <button onClick={this.searchClick} className="btn btn-outline-success col-md-2" type="button">Search</button>
            </form>
        )
    }
}

SearchBar.propTypes = {
    searchHandler: Proptypes.func.isRequired
};

export default SearchBar;
