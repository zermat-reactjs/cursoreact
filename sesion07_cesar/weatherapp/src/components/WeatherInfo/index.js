import React, {Component} from 'react';
import "./weatherInfo.css";

class WeatherInfo extends Component {


    render() {
        const estilo = {color: "#f00"};

        return (
            <div className="card">
                <img src="..." className="card-img-top" alt="..."/>
                    <div className="card-body">
                        <h5 className="card-title" style={estilo}>{this.props.weather.name}</h5>
                        <p className={ this.props.weather.name == 'Cancun' ? 'redText' : ''  }>Some quick example text to build on the card title and make up the bulk
                            of the card's content.</p>
                        <a href="#" className="btn btn-primary">Go somewhere</a>
                    </div>
            </div>
        )
    }
}

export default WeatherInfo;
