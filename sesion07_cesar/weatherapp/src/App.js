import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
import SearchBar from "./components/SearchBar";
import WeatherInfo from "./components/WeatherInfo";

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      weather: {}
    };

    this.searchCity = this.searchCity.bind(this);
  }

  searchCity (city) {
    const url = "http://api.openweathermap.org/data/2.5/weather?units=metric&lang=es&apikey=dc7afa473d1ae60c2917b26228818662&q=" + city;
    axios.get(url).then( resp => this.setState({weather: resp.data}) );
  }

  render() {
    return (
        <div className="App container">
          <SearchBar searchHandler={this.searchCity}/>
          { this.state.weather && <WeatherInfo weather={this.state.weather}/>}
        </div>
    );
  }


}

export default App;
