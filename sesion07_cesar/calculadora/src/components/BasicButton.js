import React, {Component} from 'react';

class BasicButton extends Component {
    constructor (props) {
        super(props);

        this.onClickHandler = this.onClickHandler.bind(this);
    }

    static defaultProps = {
        text: 'Basic button',
        on_click: () => {console.log('Clicked!')},
        disabled: false
    };

    onClickHandler () {
        this.props.on_click( this.props.text );
    }

    render() {
        return <button disabled={this.props.disabled} onClick={this.onClickHandler}>{this.props.text}</button>
    }
}

export default BasicButton;
