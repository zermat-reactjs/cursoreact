import React, {Component} from 'react';
import BasicButton from "./BasicButton";

class Calculator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            numberA: null,
            numberB: null,
            operator: null,
            result: null
        };

        this.addNumber = this.addNumber.bind(this);
        this.addOperator = this.addOperator.bind(this);
        this.calculateResult = this.calculateResult.bind(this);
        this.display = this.display.bind(this);
    }

    display () {
        const {numberA, numberB, operator, result} = this.state;
        return result ? result : (numberA || '') + ' ' + (operator || '') + ' ' + (numberB || '');
    }

    calculateResult () {
        let result = null;
        switch (this.state.operator) {
            case '+':
                result = parseInt(this.state.numberA) + parseInt(this.state.numberB);
                break;
            case '-':
                result = parseInt(this.state.numberA) - parseInt(this.state.numberB);
                break;
            case 'x':
                result = parseInt(this.state.numberA) * parseInt(this.state.numberB);
                break;
            case '/':
                result = parseInt(this.state.numberA) / parseInt(this.state.numberB);
                break;
            default:
                console.log('hello');
                break;
        }

        this.setState({result: result});
    }

    addOperator (symbol) {
        this.setState({operator: symbol});

        if (symbol === '=') {
            this.calculateResult();
        }
    }

    addNumber (number) {
        if ( this.state.operator ) {
            const newNumber = this.state.numberB ? this.state.numberB.toString() + number.toString() : number.toString();
            this.setState({numberB: newNumber});
            console.log('Numero B', this.state.numberB);
        } else {
            const newNumber = this.state.numberA ? this.state.numberA.toString() + number.toString() : number.toString();
            this.setState({numberA: newNumber});
            console.log('Numero A', this.state.numberB);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('State Previo:', prevState);
        console.log('Actual state:', this.state);
    }

    render() {
        const {numberA, numberB, operator} = this.state;
        return (<div>
            <input type="text" disabled value={this.display()}/>
            <BasicButton on_click={this.addNumber} text="0"/>
            <BasicButton on_click={this.addNumber} text="1"/>
            <BasicButton on_click={this.addNumber} text="2"/>
            <BasicButton on_click={this.addNumber} text="3"/>
            <BasicButton on_click={this.addNumber} text="4"/>
            <BasicButton on_click={this.addNumber} text="5"/>
            <BasicButton on_click={this.addNumber} text="6"/>
            <BasicButton on_click={this.addNumber} text="7"/>
            <BasicButton on_click={this.addNumber} text="8"/>
            <BasicButton on_click={this.addNumber} text="9"/>
            <BasicButton on_click={this.addOperator} text="+"/>
            <BasicButton on_click={this.addOperator} text="-"/>
            <BasicButton on_click={this.addOperator} text="X"/>
            <BasicButton on_click={this.addOperator} text="/"/>

            <BasicButton disabled={ !Boolean( this.state.numberA && this.state.numberB && this.state.operator ) } on_click={this.addOperator} text="="/>
        </div>)
    }
}

export default Calculator;