import React, {Component} from 'react';
import {connect} from 'react-redux';
import {changeName} from '../redux/actionCreators';

const mapStateToProps = state => {
    return {
        cosa: state.name
    }
};

const mapDispatchToProps = dispatch => {
    return {
        accionDeBoton: (newName) => {
            dispatch(changeName(newName));
        },
        accionDeBoton_2: () => {
            dispatch({type: "HOLA"})
        },
    }
};

class Title extends Component {
    constructor (props) {
        super(props);
        this.state = {
            username: ''
        };

        this.changeName = this.changeName.bind(this);
        this.enviarNombre = this.enviarNombre.bind(this);
    }

    changeName (evt) {
        const name = evt.target.value;
        this.setState({username: name});
    }

    enviarNombre () {
        this.props.accionDeBoton( this.state.username );
    }

    render() {
        return (
            <React.Fragment>
                <h1>{this.props.cosa}</h1>
                <input type="text" onChange={ this.changeName } />
                <br/>
                <button type="button" onClick={this.enviarNombre}>Dispatch!</button>
                <button type="button" onClick={this.props.accionDeBoton_2}>Dispatch!</button>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Title);
