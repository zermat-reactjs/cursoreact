import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from 'react-redux';
import store from "./redux/configureStore";
import Title from "./components/Title";

store.subscribe(() => console.log('STORE CHANGED', store.getState()) );

console.log('STORE: ', store);
console.log('STORE STATE: ', store.getState());

const {name} = store.getState();
console.log('name:', name);

store.dispatch({type: "ADD_EMAIL", payload: "cesargy@dedica.com.mx"});
store.dispatch({type: "CHANGE_NAME", payload: "Leonardo"});

setTimeout(function () {
  store.dispatch({type: "CHANGE_NAME", payload: "Karla"});
}, 10000);

function App() {
  return (
      <Provider store={store}>
        <div className="App">
          <Title/>
        </div>
      </Provider>
  );
}

export default App;
