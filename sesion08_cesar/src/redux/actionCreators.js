import {CHANGE_NAME, ADD_EMAIL} from "./actionTypes";


export const addEmail = (email) => {
    return { type: ADD_EMAIL, payload: email };
};

export const changeName = (name) => {
    return { type: CHANGE_NAME, payload: name };
};


