import { createStore } from 'redux';
import basicReducer from "./basicReducer";

const store = createStore(basicReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;
