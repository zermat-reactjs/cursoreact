import {ADD_EMAIL, CHANGE_NAME} from "./actionTypes";

const initialState = {
    name: "",
    age: null
};

const basicReducer = (state = initialState, action) => {
    console.log('Basic reducer: ', state, action);
    const {type, payload} = action;

    switch (type) {
        case ADD_EMAIL:
            return  {...state, email: payload};
        case CHANGE_NAME:
            return  {...state, name: payload};
        default:
            return  state;
    }
};

export default basicReducer;