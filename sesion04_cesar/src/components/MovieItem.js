import React, {Component} from 'react';
import {MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBBtn, MDBCardTitle} from "mdbreact";


class MovieItem extends Component{
    constructor (props) {
        super(props)

        this.state = {
            msg: ''
        }
        this.updateMsg = this.updateMsg.bind(this);
    }

    componentDidMount() {
        this.updateMsg();
    }

    updateMsg () {
        const msg = parseInt(this.props.movie.Year) < 2010 ? 'Old movie' : 'New Movie';
        console.log('---', msg)
        this.setState({msg: msg})
    }

    render() {
        const {Title, Year, Poster} = this.props.movie;
        return (
            <MDBCard md="4" style={{ width: "22rem" }}>
                <MDBCardImage className="img-fluid" src={Poster} waves />
                <MDBCardBody>
                    <MDBCardTitle>{Title}</MDBCardTitle>
                    <MDBCardText>{Year}</MDBCardText>
                    <MDBBtn href="#">MDBBtn</MDBBtn>
                </MDBCardBody>
            </MDBCard>
        )
    }
}

export default MovieItem;
